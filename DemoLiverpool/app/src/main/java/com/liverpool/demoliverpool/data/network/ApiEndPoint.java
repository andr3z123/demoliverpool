package com.liverpool.demoliverpool.data.network;

import com.liverpool.demoliverpool.BuildConfig;

/**
 * Created by andresaleman on 9/15/17.
 */

public final class ApiEndPoint
{
    public static final String ENDPOINT_PRODUCTS = BuildConfig.BASE_URL;
    public static final String ENDPOINT_PRODUCTS_2="&d3106047a194921c01969dfdec083925=json";

    private ApiEndPoint() {

    }
}
