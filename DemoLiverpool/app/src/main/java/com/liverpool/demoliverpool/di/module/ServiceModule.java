package com.liverpool.demoliverpool.di.module;

import android.app.Service;

import dagger.Module;

/**
 * Created by andresaleman on 9/15/17.
 */

@Module
public class ServiceModule {

    private final Service mService;

    public ServiceModule(Service service) {
        mService = service;
    }
}

