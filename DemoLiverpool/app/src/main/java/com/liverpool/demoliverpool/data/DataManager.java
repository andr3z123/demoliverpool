package com.liverpool.demoliverpool.data;

import com.liverpool.demoliverpool.data.db.DbHelper;
import com.liverpool.demoliverpool.data.network.ApiHelper;

/**
 * Created by andresaleman on 9/15/17.
 */

public interface DataManager extends DbHelper, ApiHelper {
}
