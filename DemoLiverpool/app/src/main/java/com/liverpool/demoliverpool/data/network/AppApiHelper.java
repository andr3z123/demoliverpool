package com.liverpool.demoliverpool.data.network;

import com.liverpool.demoliverpool.data.network.model.ProductsResponse;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by andresaleman on 9/15/17.
 */

@Singleton
public class AppApiHelper implements ApiHelper
{

    private ApiHeader mApiHeader;

    @Inject
    public AppApiHelper(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public Observable<ProductsResponse> getProductsApiCall(String standard) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_PRODUCTS+standard+ ApiEndPoint.ENDPOINT_PRODUCTS_2)
                .build()
                .getObjectObservable(ProductsResponse.class);
    }
}
