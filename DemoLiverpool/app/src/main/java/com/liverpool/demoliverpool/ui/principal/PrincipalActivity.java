package com.liverpool.demoliverpool.ui.principal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.liverpool.demoliverpool.R;
import com.liverpool.demoliverpool.ui.base.BaseActivity;
import com.liverpool.demoliverpool.ui.principal.productos.ProductosFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PrincipalActivity extends BaseActivity implements PrincipalMvpView
{
    @Inject
    PrincipalMvpPresenter<PrincipalMvpView> mPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.feed_view_pager)
    ViewPager mViewPager;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, PrincipalActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pricipal);
        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);
        setUp();
        showProducts();
    }

    @Override
    protected void setUp()
    {
        setSupportActionBar(mToolbar);
    }

    @Override
    public void showProducts()
    {
        getSupportFragmentManager()
                .beginTransaction()
                .disallowAddToBackStack()
                .add(R.id.cl_root_view, ProductosFragment.newInstance(), ProductosFragment.TAG)
                .commit();
    }
}
