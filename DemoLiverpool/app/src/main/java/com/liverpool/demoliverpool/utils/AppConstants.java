package com.liverpool.demoliverpool.utils;

/**
 * Created by andresaleman on 9/15/17.
 */

public class AppConstants
{
    public static final String STATUS_CODE_SUCCESS = "success";
    public static final String STATUS_CODE_FAILED = "failed";

    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;

    public static final String DB_NAME = "mindorks_mvp.db";
    public static final String PREF_NAME = "mindorks_pref";

    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}
