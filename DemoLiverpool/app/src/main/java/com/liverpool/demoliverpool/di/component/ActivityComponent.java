package com.liverpool.demoliverpool.di.component;

import com.liverpool.demoliverpool.di.PerActivity;
import com.liverpool.demoliverpool.di.module.ActivityModule;
import com.liverpool.demoliverpool.ui.principal.PrincipalActivity;
import com.liverpool.demoliverpool.ui.principal.productos.ProductosFragment;

import dagger.Component;

/**
 * Created by andresaleman on 9/15/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent  {

    void inject(PrincipalActivity activity);

    void inject(ProductosFragment fragment);
}
