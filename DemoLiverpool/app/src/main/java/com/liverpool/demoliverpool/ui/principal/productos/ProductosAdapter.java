package com.liverpool.demoliverpool.ui.principal.productos;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.liverpool.demoliverpool.R;
import com.liverpool.demoliverpool.data.network.model.ProductsResponse;
import com.liverpool.demoliverpool.ui.base.BaseViewHolder;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by andresaleman on 9/15/17.
 */

public class ProductosAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<ProductsResponse.Records> mContentResponseList;

    public ProductosAdapter(List<ProductsResponse.Records> contentResponseList) {
        mContentResponseList = contentResponseList;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_products_view, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mContentResponseList != null && mContentResponseList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mContentResponseList != null && mContentResponseList.size() > 0) {
            return mContentResponseList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<ProductsResponse.Records> contentList) {
        mContentResponseList.addAll(contentList);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onRepoEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {

        @BindView(R.id.cover_image_view)
        ImageView coverImageView;

        @BindView(R.id.price_text_view)
        TextView priceTextView;

        @BindView(R.id.location_text_view)
        TextView locationTextView;

        @BindView(R.id.title_text_view)
        TextView titleTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear() {
            coverImageView.setImageDrawable(null);
            priceTextView.setText("");
            titleTextView.setText("");
        }

        public void onBind(int position)
        {
            super.onBind(position);

            final List<ProductsResponse.Record> content = mContentResponseList.get(position).getRecords();
            for (ProductsResponse.Record records: content)
            {
                if (records.getAttributes().getProductSmallImage() != null) {
                    Glide.with(itemView.getContext())
                            .load(records.getAttributes().getProductSmallImage()[0].toString())
                            .into(coverImageView);
                }

                if (records.getAttributes().getProductDisplayName() != null) {
                    titleTextView.setText(records.getAttributes().getProductDisplayName()[0].toString());
                }

                if (records.getAttributes().getSkuListPrice() != null) {
                    priceTextView.setText("$ "+records.getAttributes().getSkuListPrice()[0].toString());
                }
            }
        }
    }

    public void clear()
    {
        mContentResponseList.clear();
    }

    public class EmptyViewHolder extends BaseViewHolder
    {
        @BindView(R.id.tv_message)
        TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }
    }
}