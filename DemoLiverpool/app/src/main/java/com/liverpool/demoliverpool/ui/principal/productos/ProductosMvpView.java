package com.liverpool.demoliverpool.ui.principal.productos;

import com.liverpool.demoliverpool.data.network.model.ProductsResponse;
import com.liverpool.demoliverpool.ui.base.MvpView;

import java.util.List;

/**
 * Created by andresaleman on 9/15/17.
 */

public interface ProductosMvpView extends MvpView {

    void updateRepo(List<ProductsResponse.Records> contentList);
}

