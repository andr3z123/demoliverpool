package com.liverpool.demoliverpool.data;

import android.content.Context;

import com.liverpool.demoliverpool.data.db.DbHelper;
import com.liverpool.demoliverpool.data.network.ApiHelper;
import com.liverpool.demoliverpool.data.network.model.ProductsResponse;
import com.liverpool.demoliverpool.di.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by andresaleman on 9/15/17.
 */

@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final DbHelper mDbHelper;
    private final ApiHelper mApiHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context, DbHelper dbHelper, ApiHelper apiHelper) {
        mContext = context;
        mDbHelper = dbHelper;
        mApiHelper = apiHelper;
    }

    @Override
    public Observable<ProductsResponse> getProductsApiCall(String standard) {
        return mApiHelper.getProductsApiCall(standard);
    }
}
