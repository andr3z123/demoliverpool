package com.liverpool.demoliverpool.ui.principal.productos;

import android.text.TextUtils;

import com.androidnetworking.error.ANError;
import com.google.gson.reflect.TypeToken;
import com.liverpool.demoliverpool.data.DataManager;
import com.liverpool.demoliverpool.data.network.model.ProductsResponse;
import com.liverpool.demoliverpool.ui.base.BasePresenter;
import com.liverpool.demoliverpool.utils.rx.SchedulerProvider;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by andresaleman on 9/15/17.
 */

public class ProductosPresenter<V extends ProductosMvpView> extends BasePresenter<V>
        implements ProductosMvpPresenter<V>
{
    @Inject
    public ProductosPresenter(DataManager dataManager,
                                        SchedulerProvider schedulerProvider,
                                        CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared(String standard)
    {
        getCompositeDisposable().add(getDataManager()
                .getProductsApiCall(standard)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<ProductsResponse>() {
                    @Override
                    public void accept(@NonNull ProductsResponse productsResponse)
                            throws Exception {
                        if (productsResponse != null && productsResponse.getContents() != null) {
                            //getMvpView().updateRepo(productsResponse.getContents()[0].getMainContent()[productsResponse.getContents()[0].getMainContent().length - 1].getContents()[0].getRecords());
                            getMvpView().updateRepo(productsResponse.getContents()[0].getMainContent()[productsResponse.getContents()[0].getMainContent().length - 1].getContents()[0].getRecords());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        // handle the error here
                        if (throwable instanceof ANError)
                        {
                            ANError anError = (ANError) throwable;
                            //handleApiError(anError);
                        }
                    }
                }));
    }
}