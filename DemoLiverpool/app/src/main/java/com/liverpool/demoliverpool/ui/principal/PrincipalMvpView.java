package com.liverpool.demoliverpool.ui.principal;

import com.liverpool.demoliverpool.ui.base.MvpView;

/**
 * Created by andresaleman on 9/15/17.
 */

public interface PrincipalMvpView extends MvpView
{
    void showProducts();
}
