package com.liverpool.demoliverpool.di.component;

import android.app.Application;
import android.content.Context;

import com.liverpool.demoliverpool.MvpApp;
import com.liverpool.demoliverpool.data.DataManager;
import com.liverpool.demoliverpool.di.ApplicationContext;
import com.liverpool.demoliverpool.di.module.ApplicationModule;
import com.liverpool.demoliverpool.service.SyncService;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by andresaleman on 9/15/17.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MvpApp app);

    void inject(SyncService service);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();
}
