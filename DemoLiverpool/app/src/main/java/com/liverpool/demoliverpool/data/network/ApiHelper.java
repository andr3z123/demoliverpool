package com.liverpool.demoliverpool.data.network;

import com.liverpool.demoliverpool.data.network.model.ProductsResponse;

import io.reactivex.Observable;

/**
 * Created by andresaleman on 9/15/17.
 */

public interface ApiHelper
{
    Observable<ProductsResponse> getProductsApiCall(String standard);
}
