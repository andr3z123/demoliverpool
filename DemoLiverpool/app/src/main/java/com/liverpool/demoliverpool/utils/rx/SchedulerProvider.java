package com.liverpool.demoliverpool.utils.rx;

import io.reactivex.Scheduler;

/**
 * Created by andresaleman on 9/15/17.
 */

public interface SchedulerProvider {

    Scheduler ui();

    Scheduler computation();

    Scheduler io();

}
