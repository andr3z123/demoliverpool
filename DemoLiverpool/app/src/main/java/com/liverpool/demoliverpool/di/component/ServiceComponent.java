package com.liverpool.demoliverpool.di.component;

import com.liverpool.demoliverpool.di.PerService;
import com.liverpool.demoliverpool.di.module.ServiceModule;
import com.liverpool.demoliverpool.service.SyncService;

import dagger.Component;

/**
 * Created by andresaleman on 9/15/17.
 */

@PerService
@Component(dependencies = ApplicationComponent.class, modules = ServiceModule.class)
public interface ServiceComponent {

    void inject(SyncService service);

}
