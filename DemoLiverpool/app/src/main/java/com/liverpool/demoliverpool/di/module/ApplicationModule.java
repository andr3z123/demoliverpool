package com.liverpool.demoliverpool.di.module;

import android.app.Application;
import android.content.Context;

import com.liverpool.demoliverpool.R;
import com.liverpool.demoliverpool.data.AppDataManager;
import com.liverpool.demoliverpool.data.DataManager;
import com.liverpool.demoliverpool.data.db.AppDbHelper;
import com.liverpool.demoliverpool.data.db.DbHelper;
import com.liverpool.demoliverpool.data.network.ApiHelper;
import com.liverpool.demoliverpool.data.network.AppApiHelper;
import com.liverpool.demoliverpool.di.ApplicationContext;
import com.liverpool.demoliverpool.di.DatabaseInfo;
import com.liverpool.demoliverpool.di.PreferenceInfo;
import com.liverpool.demoliverpool.utils.AppConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by andresaleman on 9/15/17.
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    DbHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }


    @Provides
    @Singleton
    CalligraphyConfig provideCalligraphyDefaultConfig() {
        return new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/source-sans-pro/SourceSansPro-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build();
    }
}