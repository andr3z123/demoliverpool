package com.liverpool.demoliverpool.ui.principal.productos;

import com.liverpool.demoliverpool.data.network.model.ProductsResponse;
import com.liverpool.demoliverpool.ui.base.MvpPresenter;

/**
 * Created by andresaleman on 9/15/17.
 */

public interface ProductosMvpPresenter <V extends ProductosMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared(String standard);
}