package com.liverpool.demoliverpool.data.db;

import com.liverpool.demoliverpool.data.db.model.DaoMaster;
import com.liverpool.demoliverpool.data.db.model.DaoSession;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by andresaleman on 9/15/17.
 */

@Singleton
public class AppDbHelper implements DbHelper {

    private final DaoSession mDaoSession;

    @Inject
    public AppDbHelper(DbOpenHelper dbOpenHelper) {
        mDaoSession = new DaoMaster(dbOpenHelper.getWritableDb()).newSession();
    }
}
