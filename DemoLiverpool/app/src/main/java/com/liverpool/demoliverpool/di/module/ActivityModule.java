package com.liverpool.demoliverpool.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.liverpool.demoliverpool.data.network.model.ProductsResponse;
import com.liverpool.demoliverpool.di.ActivityContext;
import com.liverpool.demoliverpool.di.PerActivity;
import com.liverpool.demoliverpool.ui.principal.PrincipalMvpPresenter;
import com.liverpool.demoliverpool.ui.principal.PrincipalMvpView;
import com.liverpool.demoliverpool.ui.principal.PrincipalPresenter;
import com.liverpool.demoliverpool.ui.principal.productos.ProductosAdapter;
import com.liverpool.demoliverpool.ui.principal.productos.ProductosMvpPresenter;
import com.liverpool.demoliverpool.ui.principal.productos.ProductosMvpView;
import com.liverpool.demoliverpool.ui.principal.productos.ProductosPresenter;
import com.liverpool.demoliverpool.utils.rx.AppSchedulerProvider;
import com.liverpool.demoliverpool.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by andresaleman on 9/15/17.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @PerActivity
    PrincipalMvpPresenter<PrincipalMvpView> providePrincipalPresenter(
            PrincipalPresenter<PrincipalMvpView> presenter) {
        return presenter;
    }

    @Provides
    ProductosMvpPresenter<ProductosMvpView> provideProductosPresenter(
            ProductosPresenter<ProductosMvpView> presenter) {
        return presenter;
    }

    @Provides
    ProductosAdapter provideProductosAdapter() {
        return new ProductosAdapter(new ArrayList<ProductsResponse.Records>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }
}
