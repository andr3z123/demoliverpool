package com.liverpool.demoliverpool.ui.principal.productos;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.AbstractCursor;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.liverpool.demoliverpool.R;
import com.liverpool.demoliverpool.data.network.model.ProductsResponse;
import com.liverpool.demoliverpool.di.component.ActivityComponent;
import com.liverpool.demoliverpool.ui.base.BaseFragment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductosFragment extends BaseFragment implements
        ProductosMvpView, ProductosAdapter.Callback
{
    public static final String TAG = "TopFreeApplicationsFragment";
    @Inject
    ProductosMvpPresenter<ProductosMvpView> mPresenter;
    @Inject
    ProductosAdapter mProductosAdapter;
    @Inject
    LinearLayoutManager mLayoutManager;
    @BindView(R.id.repo_recycler_view)
    RecyclerView mRecyclerView;
    private String mSearchString;
    SearchView searchViewAndroidActionBar;

    private MenuItem searchViewItem;
    private static final String SEARCH_KEY = "search";

    public static ProductosFragment newInstance() {
        Bundle args = new Bundle();
        ProductosFragment fragment = new ProductosFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_productos, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            mProductosAdapter.setCallback(this);
        }
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mSearchString = savedInstanceState.getString(SEARCH_KEY);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mSearchString = searchViewAndroidActionBar.getQuery().toString();
        outState.putString(SEARCH_KEY, mSearchString);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search, menu);
        searchViewItem = menu.findItem(R.id.action_search_simple);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);

        if (mSearchString != null && !mSearchString.isEmpty())
        {
            searchViewAndroidActionBar.setQuery(mSearchString, true);
            searchViewAndroidActionBar.clearFocus();
        }

        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                searchViewAndroidActionBar.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query)
            {
                if(query.length()>4)
                {
                    mPresenter.onViewPrepared(query);

                }
                else
                {
                    mProductosAdapter.clear();
                    mProductosAdapter.notifyDataSetChanged();
                }
                return false;
            }
        });
    }

    @Override
    public void onBack() {

    }

    @Override
    public void updateRepo(List<ProductsResponse.Records> contentList)
    {
        mProductosAdapter.addItems(contentList);
    }

    @Override
    public void onRepoEmptyViewRetryClick() {

    }

    @Override
    protected void setUp(View view)
    {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mProductosAdapter);
        mPresenter.onViewPrepared("");
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }
}
