package com.liverpool.demoliverpool.ui.principal;

import com.liverpool.demoliverpool.data.DataManager;
import com.liverpool.demoliverpool.ui.base.BasePresenter;
import com.liverpool.demoliverpool.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by andresaleman on 9/15/17.
 */

public class PrincipalPresenter<V extends PrincipalMvpView> extends BasePresenter<V> implements
        PrincipalMvpPresenter<V>
{

    private static final String TAG = "PrincipalPresenter";

    @Inject
    public PrincipalPresenter(DataManager dataManager,
                              SchedulerProvider schedulerProvider,
                              CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewInitialized() {
        getMvpView().showProducts();
    }
}
