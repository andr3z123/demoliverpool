package com.liverpool.demoliverpool.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by andresaleman on 9/15/17.
 */

public class ProductsResponse
{
    public class MainContent
    {
        @Expose
        @SerializedName("contents")
        private Contents[] contents;
        @Expose
        @SerializedName("@type")
        private String type;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("originalTerms")
        private List<String> originalTerms;
        @Expose
        @SerializedName("ruleLimit")
        private String ruleLimit;

        public Contents[] getContents() {
            return contents;
        }

        public void setContents(Contents[] contents) {
            this.contents = contents;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getOriginalTerms() {
            return originalTerms;
        }

        public void setOriginalTerms(List<String> originalTerms) {
            this.originalTerms = originalTerms;
        }

        public String getRuleLimit() {
            return ruleLimit;
        }

        public void setRuleLimit(String ruleLimit) {
            this.ruleLimit = ruleLimit;
        }

        public String getContentCollection() {
            return contentCollection;
        }

        public void setContentCollection(String contentCollection) {
            this.contentCollection = contentCollection;
        }

        @Expose
        @SerializedName("contentCollection")
        private String contentCollection;
    }

    public class Contents {
        @Expose
        @SerializedName("lastRecNum")
        private Long lastRecNum;
        @Expose
        @SerializedName("pagingActionTemplate")
        private DetailsAction pagingActionTemplate;
        @Expose
        @SerializedName("content")
        private String content;
        @Expose
        @SerializedName("@type")
        private String type;
        @Expose
        @SerializedName("firstRecNum")
        private Long firstRecNum;
        @Expose
        @SerializedName("minimumResultListPrice")
        private String minimumResultListPrice;
        @Expose
        @SerializedName("maximumResultListPrice")
        private String maximumResultListPrice;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("records")
        private List<Records> records;
        @Expose
        @SerializedName("sortOptions")
        private SortOptions[] sortOptions;
        @Expose
        @SerializedName("precomputedSorts")
        private Object[] precomputedSorts;
        @Expose
        @SerializedName("recsPerPage")
        private Long recsPerPage;
        @Expose
        @SerializedName("totalNumRecs")
        private Long totalNumRecs;


        public Long getLastRecNum() { return lastRecNum; }
        public void setLastRecNum(Long value) { this.lastRecNum = value; }


        public DetailsAction getPagingActionTemplate() { return pagingActionTemplate; }
        public void setPagingActionTemplate(DetailsAction value) { this.pagingActionTemplate = value; }


        public String getContent() { return content; }
        public void setContent(String value) { this.content = value; }


        public String getType() { return type; }
        public void setType(String value) { this.type = value; }


        public Long getFirstRecNum() { return firstRecNum; }
        public void setFirstRecNum(Long value) { this.firstRecNum = value; }


        public String getMinimumResultListPrice() { return minimumResultListPrice; }
        public void setMinimumResultListPrice(String value) { this.minimumResultListPrice = value; }


        public String getMaximumResultListPrice() { return maximumResultListPrice; }
        public void setMaximumResultListPrice(String value) { this.maximumResultListPrice = value; }


        public String getName() { return name; }
        public void setName(String value) { this.name = value; }


        public List<Records> getRecords() { return records; }
        public void setRecords(List<Records> value) { this.records = value; }


        public SortOptions[] getSortOptions() { return sortOptions; }
        public void setSortOptions(SortOptions[] value) { this.sortOptions = value; }


        public Object[] getPrecomputedSorts() { return precomputedSorts; }
        public void setPrecomputedSorts(Object[] value) { this.precomputedSorts = value; }


        public Long getRecsPerPage() { return recsPerPage; }
        public void setRecsPerPage(Long value) { this.recsPerPage = value; }


        public Long getTotalNumRecs() { return totalNumRecs; }
        public void setTotalNumRecs(Long value) { this.totalNumRecs = value; }
    }

    public class DetailsAction {
        @Expose
        @SerializedName("contentPath")
        private String contentPath;
        @Expose
        @SerializedName("navigationState")
        private String navigationState;
        @Expose
        @SerializedName("otherClass")
        private String otherClass;
        @Expose
        @SerializedName("label")
        private Object label;
        @Expose
        @SerializedName("recordState")
        private String recordState;
        @Expose
        @SerializedName("siteRootPath")
        private String siteRootPath;


        public String getContentPath() { return contentPath; }
        public void setContentPath(String value) { this.contentPath = value; }

        public String getNavigationState() { return navigationState; }
        public void setNavigationState(String value) { this.navigationState = value; }

        public String getOtherClass() { return otherClass; }
        public void setOtherClass(String value) { this.otherClass = value; }

        public Object getLabel() { return label; }
        public void setLabel(Object value) { this.label = value; }

        public String getRecordState() { return recordState; }
        public void setRecordState(String value) { this.recordState = value; }

        public String getSiteRootPath() { return siteRootPath; }
        public void setSiteRootPath(String value) { this.siteRootPath = value; }
    }

    public class Records {
        @Expose
        @SerializedName("attributes")
        private Attributes attributes;
        @Expose
        @SerializedName("numRecords")
        private long numRecords;
        @Expose
        @SerializedName("otherClass")
        private String otherClass;
        @Expose
        @SerializedName("detailsAction")
        private DetailsAction detailsAction;
        @Expose
        @SerializedName("records")
        private List<Record> records;

        public Attributes getAttributes() { return attributes; }
        public void setAttributes(Attributes value) { this.attributes = value; }

        public long getNumRecords() { return numRecords; }
        public void setNumRecords(long value) { this.numRecords = value; }

        public String getOtherClass() { return otherClass; }
        public void setOtherClass(String value) { this.otherClass = value; }

        public DetailsAction getDetailsAction() { return detailsAction; }
        public void setDetailsAction(DetailsAction value) { this.detailsAction = value; }

        public List<Record> getRecords() { return records; }
        public void setRecords(List<Record> value) { this.records = value; }
    }

    public class Attributes {
        @Expose
        @SerializedName("isExclusivePromotion")
        private String[] isExclusivePromotion;
        @Expose
        @SerializedName("exclusiveProductEndDate")
        private String[] exclusiveProductEndDate;
        @Expose
        @SerializedName("deliveryOnlyStoreEndDate")
        private String[] deliveryOnlyStoreEndDate;
        @Expose
        @SerializedName("ancestorCategoriesDisplayName")
        private String[] ancestorCategoriesDisplayName;
        @Expose
        @SerializedName("language")
        private String[] language;
        @Expose
        @SerializedName("inventoryStatus")
        private String[] inventoryStatus;
        @Expose
        @SerializedName("allAncestorsRepositoryId")
        private String[] allAncestorsRepositoryId;
        @Expose
        @SerializedName("availabilityShopStartDate")
        private String[] availabilityShopStartDate;
        @Expose
        @SerializedName("availabilityShopEndDate")
        private String[] availabilityShopEndDate;
        @Expose
        @SerializedName("common.id")
        private String[] commonId;
        @Expose
        @SerializedName("exclusivePackageEndDate")
        private String[] exclusivePackageEndDate;
        @Expose
        @SerializedName("exclusivDiscountEndDate")
        private String[] exclusivDiscountEndDate;
        @Expose
        @SerializedName("deliveryOnlyStoreStartDate")
        private String[] deliveryOnlyStoreStartDate;
        @Expose
        @SerializedName("exclusivDiscountStartDate")
        private String[] exclusivDiscountStartDate;
        @Expose
        @SerializedName("exclusivePriceEndDate")
        private String[] exclusivePriceEndDate;
        @Expose
        @SerializedName("exclusivePackageStartDate")
        private String[] exclusivePackageStartDate;
        @Expose
        @SerializedName("exclusivePriceStartDate")
        private String[] exclusivePriceStartDate;
        @Expose
        @SerializedName("groupType")
        private String[] groupType;
        @Expose
        @SerializedName("expressDeliveryEndDate")
        private String[] expressDeliveryEndDate;
        @Expose
        @SerializedName("exclusivePromotionEndDate")
        private String[] exclusivePromotionEndDate;
        @Expose
        @SerializedName("exclusiveProductStartDate")
        private String[] exclusiveProductStartDate;
        @Expose
        @SerializedName("exclusivePromotionStartDate")
        private String[] exclusivePromotionStartDate;
        @Expose
        @SerializedName("giftWithPurchaseEndDate")
        private String[] giftWithPurchaseEndDate;
        @Expose
        @SerializedName("expressDeliveryStartDate")
        private String[] expressDeliveryStartDate;
        @Expose
        @SerializedName("giftWithPurchaseStartDate")
        private String[] giftWithPurchaseStartDate;
        @Expose
        @SerializedName("isExclusivDiscount")
        private String[] isExclusivDiscount;
        @Expose
        @SerializedName("isAvailabilityShop")
        private String[] isAvailabilityShop;
        @Expose
        @SerializedName("importBook")
        private String[] importBook;
        @Expose
        @SerializedName("isDeliveryOnlyStore")
        private String[] isDeliveryOnlyStore;
        @Expose
        @SerializedName("isExclusivePrice")
        private String[] isExclusivePrice;
        @Expose
        @SerializedName("isExclusivePackage")
        private String[] isExclusivePackage;
        @Expose
        @SerializedName("isExclusiveProduct")
        private String[] isExclusiveProduct;
        @Expose
        @SerializedName("minimumPromoPrice")
        private String[] minimumPromoPrice;
        @Expose
        @SerializedName("latestPartsEndDate")
        private String[] latestPartsEndDate;
        @Expose
        @SerializedName("isLimitedPieces")
        private String[] isLimitedPieces;
        @Expose
        @SerializedName("isGiftWithPurchase")
        private String[] isGiftWithPurchase;
        @Expose
        @SerializedName("isExpressDelivery")
        private String[] isExpressDelivery;
        @Expose
        @SerializedName("isLatestParts")
        private String[] isLatestParts;
        @Expose
        @SerializedName("isPresale")
        private String[] isPresale;
        @Expose
        @SerializedName("isNewProduct")
        private String[] isNewProduct;
        @Expose
        @SerializedName("isShipPleasant")
        private String[] isShipPleasant;
        @Expose
        @SerializedName("materialGroup")
        private String[] materialGroup;
        @Expose
        @SerializedName("limitedPiecesEndDate")
        private String[] limitedPiecesEndDate;
        @Expose
        @SerializedName("latestPartsStartDate")
        private String[] latestPartsStartDate;
        @Expose
        @SerializedName("limitedPiecesStartDate")
        private String[] limitedPiecesStartDate;
        @Expose
        @SerializedName("maximumPromoPrice")
        private String[] maximumPromoPrice;
        @Expose
        @SerializedName("maximumListPrice")
        private String[] maximumListPrice;
        @Expose
        @SerializedName("minimumListPrice")
        private String[] minimumListPrice;
        @Expose
        @SerializedName("product.displayName")
        private String[] productDisplayName;
        @Expose
        @SerializedName("productAvgRating")
        private String[] productAvgRating;
        @Expose
        @SerializedName("presaleEndDate")
        private String[] presaleEndDate;
        @Expose
        @SerializedName("newProductStartDate")
        private String[] newProductStartDate;
        @Expose
        @SerializedName("newProductEndDate")
        private String[] newProductEndDate;
        @Expose
        @SerializedName("noRefund")
        private String[] noRefund;
        @Expose
        @SerializedName("product.brand")
        private String[] productBrand;
        @Expose
        @SerializedName("presaleStartDate")
        private String[] presaleStartDate;
        @Expose
        @SerializedName("product.description")
        private String[] productDescription;
        @Expose
        @SerializedName("product.repositoryId")
        private String[] productRepositoryId;
        @Expose
        @SerializedName("product.productFlags")
        private String[] productProductFlags;
        @Expose
        @SerializedName("product.largeImage")
        private String[] productLargeImage;
        @Expose
        @SerializedName("product.productRatingCount")
        private String[] productProductRatingCount;
        @Expose
        @SerializedName("product.swatchColors")
        private String[] productSwatchColors;
        @Expose
        @SerializedName("product.smallImage")
        private String[] productSmallImage;
        @Expose
        @SerializedName("product.videoLink")
        private String[] productVideoLink;
        @Expose
        @SerializedName("skipInventory")
        private String[] skipInventory;
        @Expose
        @SerializedName("sku.list_Price")
        private String[] skuListPrice;
        @Expose
        @SerializedName("shipPleasantEndDate")
        private String[] shipPleasantEndDate;
        @Expose
        @SerializedName("productType")
        private String[] productType;
        @Expose
        @SerializedName("shipPleasantStartDate")
        private String[] shipPleasantStartDate;
        @Expose
        @SerializedName("sku.galleriaImage")
        private String[] skuGalleriaImage;
        @Expose
        @SerializedName("sku.color")
        private String[] skuColor;
        @Expose
        @SerializedName("sku.largeImage")
        private String[] skuLargeImage;
        @Expose
        @SerializedName("sku.repositoryId")
        private String[] skuRepositoryId;
        @Expose
        @SerializedName("sku.smallImage")
        private String[] skuSmallImage;
        @Expose
        @SerializedName("sku.promoPrice")
        private String[] skuPromoPrice;
        @Expose
        @SerializedName("sku.sale_Price")
        private String[] skuSalePrice;
        @Expose
        @SerializedName("sku.thumbnailImage")
        private String[] skuThumbnailImage;
        @Expose
        @SerializedName("sortPrice")
        private String[] sortPrice;

        public String[] getIsExclusivePromotion() { return isExclusivePromotion; }
        public void setIsExclusivePromotion(String[] value) { this.isExclusivePromotion = value; }


        public String[] getExclusiveProductEndDate() { return exclusiveProductEndDate; }
        public void setExclusiveProductEndDate(String[] value) { this.exclusiveProductEndDate = value; }


        public String[] getDeliveryOnlyStoreEndDate() { return deliveryOnlyStoreEndDate; }
        public void setDeliveryOnlyStoreEndDate(String[] value) { this.deliveryOnlyStoreEndDate = value; }


        public String[] getAncestorCategoriesDisplayName() { return ancestorCategoriesDisplayName; }
        public void setAncestorCategoriesDisplayName(String[] value) { this.ancestorCategoriesDisplayName = value; }


        public String[] getLanguage() { return language; }
        public void setLanguage(String[] value) { this.language = value; }


        public String[] getInventoryStatus() { return inventoryStatus; }
        public void setInventoryStatus(String[] value) { this.inventoryStatus = value; }


        public String[] getAllAncestorsRepositoryId() { return allAncestorsRepositoryId; }
        public void setAllAncestorsRepositoryId(String[] value) { this.allAncestorsRepositoryId = value; }


        public String[] getAvailabilityShopStartDate() { return availabilityShopStartDate; }
        public void setAvailabilityShopStartDate(String[] value) { this.availabilityShopStartDate = value; }

        public String[] getAvailabilityShopEndDate() { return availabilityShopEndDate; }
        public void setAvailabilityShopEndDate(String[] value) { this.availabilityShopEndDate = value; }


        public String[] getCommonId() { return commonId; }
        public void setCommonId(String[] value) { this.commonId = value; }


        public String[] getExclusivePackageEndDate() { return exclusivePackageEndDate; }
        public void setExclusivePackageEndDate(String[] value) { this.exclusivePackageEndDate = value; }


        public String[] getExclusivDiscountEndDate() { return exclusivDiscountEndDate; }
        public void setExclusivDiscountEndDate(String[] value) { this.exclusivDiscountEndDate = value; }


        public String[] getDeliveryOnlyStoreStartDate() { return deliveryOnlyStoreStartDate; }
        public void setDeliveryOnlyStoreStartDate(String[] value) { this.deliveryOnlyStoreStartDate = value; }


        public String[] getExclusivDiscountStartDate() { return exclusivDiscountStartDate; }
        public void setExclusivDiscountStartDate(String[] value) { this.exclusivDiscountStartDate = value; }


        public String[] getExclusivePriceEndDate() { return exclusivePriceEndDate; }
        public void setExclusivePriceEndDate(String[] value) { this.exclusivePriceEndDate = value; }


        public String[] getExclusivePackageStartDate() { return exclusivePackageStartDate; }
        public void setExclusivePackageStartDate(String[] value) { this.exclusivePackageStartDate = value; }


        public String[] getExclusivePriceStartDate() { return exclusivePriceStartDate; }
        public void setExclusivePriceStartDate(String[] value) { this.exclusivePriceStartDate = value; }


        public String[] getGroupType() { return groupType; }
        public void setGroupType(String[] value) { this.groupType = value; }


        public String[] getExpressDeliveryEndDate() { return expressDeliveryEndDate; }
        public void setExpressDeliveryEndDate(String[] value) { this.expressDeliveryEndDate = value; }


        public String[] getExclusivePromotionEndDate() { return exclusivePromotionEndDate; }
        public void setExclusivePromotionEndDate(String[] value) { this.exclusivePromotionEndDate = value; }


        public String[] getExclusiveProductStartDate() { return exclusiveProductStartDate; }
        public void setExclusiveProductStartDate(String[] value) { this.exclusiveProductStartDate = value; }


        public String[] getExclusivePromotionStartDate() { return exclusivePromotionStartDate; }
        public void setExclusivePromotionStartDate(String[] value) { this.exclusivePromotionStartDate = value; }


        public String[] getGiftWithPurchaseEndDate() { return giftWithPurchaseEndDate; }
        public void setGiftWithPurchaseEndDate(String[] value) { this.giftWithPurchaseEndDate = value; }


        public String[] getExpressDeliveryStartDate() { return expressDeliveryStartDate; }
        public void setExpressDeliveryStartDate(String[] value) { this.expressDeliveryStartDate = value; }


        public String[] getGiftWithPurchaseStartDate() { return giftWithPurchaseStartDate; }
        public void setGiftWithPurchaseStartDate(String[] value) { this.giftWithPurchaseStartDate = value; }


        public String[] getIsExclusivDiscount() { return isExclusivDiscount; }
        public void setIsExclusivDiscount(String[] value) { this.isExclusivDiscount = value; }


        public String[] getIsAvailabilityShop() { return isAvailabilityShop; }
        public void setIsAvailabilityShop(String[] value) { this.isAvailabilityShop = value; }


        public String[] getImportBook() { return importBook; }
        public void setImportBook(String[] value) { this.importBook = value; }


        public String[] getIsDeliveryOnlyStore() { return isDeliveryOnlyStore; }
        public void setIsDeliveryOnlyStore(String[] value) { this.isDeliveryOnlyStore = value; }

        public String[] getIsExclusivePrice() { return isExclusivePrice; }
        public void setIsExclusivePrice(String[] value) { this.isExclusivePrice = value; }

        public String[] getIsExclusivePackage() { return isExclusivePackage; }
        public void setIsExclusivePackage(String[] value) { this.isExclusivePackage = value; }

        public String[] getIsExclusiveProduct() { return isExclusiveProduct; }
        public void setIsExclusiveProduct(String[] value) { this.isExclusiveProduct = value; }

        public String[] getMinimumPromoPrice() { return minimumPromoPrice; }
        public void setMinimumPromoPrice(String[] value) { this.minimumPromoPrice = value; }

        public String[] getLatestPartsEndDate() { return latestPartsEndDate; }
        public void setLatestPartsEndDate(String[] value) { this.latestPartsEndDate = value; }

        public String[] getIsLimitedPieces() { return isLimitedPieces; }
        public void setIsLimitedPieces(String[] value) { this.isLimitedPieces = value; }

        public String[] getIsGiftWithPurchase() { return isGiftWithPurchase; }
        public void setIsGiftWithPurchase(String[] value) { this.isGiftWithPurchase = value; }

        public String[] getIsExpressDelivery() { return isExpressDelivery; }
        public void setIsExpressDelivery(String[] value) { this.isExpressDelivery = value; }

        public String[] getIsLatestParts() { return isLatestParts; }
        public void setIsLatestParts(String[] value) { this.isLatestParts = value; }

        public String[] getIsPresale() { return isPresale; }
        public void setIsPresale(String[] value) { this.isPresale = value; }

        public String[] getIsNewProduct() { return isNewProduct; }
        public void setIsNewProduct(String[] value) { this.isNewProduct = value; }

        public String[] getIsShipPleasant() { return isShipPleasant; }
        public void setIsShipPleasant(String[] value) { this.isShipPleasant = value; }

        public String[] getMaterialGroup() { return materialGroup; }
        public void setMaterialGroup(String[] value) { this.materialGroup = value; }

        public String[] getLimitedPiecesEndDate() { return limitedPiecesEndDate; }
        public void setLimitedPiecesEndDate(String[] value) { this.limitedPiecesEndDate = value; }

        public String[] getLatestPartsStartDate() { return latestPartsStartDate; }
        public void setLatestPartsStartDate(String[] value) { this.latestPartsStartDate = value; }

        public String[] getLimitedPiecesStartDate() { return limitedPiecesStartDate; }
        public void setLimitedPiecesStartDate(String[] value) { this.limitedPiecesStartDate = value; }

        public String[] getMaximumPromoPrice() { return maximumPromoPrice; }
        public void setMaximumPromoPrice(String[] value) { this.maximumPromoPrice = value; }

        public String[] getMaximumListPrice() { return maximumListPrice; }
        public void setMaximumListPrice(String[] value) { this.maximumListPrice = value; }

        public String[] getMinimumListPrice() { return minimumListPrice; }
        public void setMinimumListPrice(String[] value) { this.minimumListPrice = value; }

        public String[] getProductDisplayName() { return productDisplayName; }
        public void setProductDisplayName(String[] value) { this.productDisplayName = value; }

        public String[] getProductAvgRating() { return productAvgRating; }
        public void setProductAvgRating(String[] value) { this.productAvgRating = value; }

        public String[] getPresaleEndDate() { return presaleEndDate; }
        public void setPresaleEndDate(String[] value) { this.presaleEndDate = value; }

        public String[] getNewProductStartDate() { return newProductStartDate; }
        public void setNewProductStartDate(String[] value) { this.newProductStartDate = value; }

        public String[] getNewProductEndDate() { return newProductEndDate; }
        public void setNewProductEndDate(String[] value) { this.newProductEndDate = value; }

        public String[] getNoRefund() { return noRefund; }
        public void setNoRefund(String[] value) { this.noRefund = value; }

        public String[] getProductBrand() { return productBrand; }
        public void setProductBrand(String[] value) { this.productBrand = value; }

        public String[] getPresaleStartDate() { return presaleStartDate; }
        public void setPresaleStartDate(String[] value) { this.presaleStartDate = value; }

        public String[] getProductDescription() { return productDescription; }
        public void setProductDescription(String[] value) { this.productDescription = value; }

        public String[] getProductRepositoryId() { return productRepositoryId; }
        public void setProductRepositoryId(String[] value) { this.productRepositoryId = value; }

        public String[] getProductProductFlags() { return productProductFlags; }
        public void setProductProductFlags(String[] value) { this.productProductFlags = value; }

        public String[] getProductLargeImage() { return productLargeImage; }
        public void setProductLargeImage(String[] value) { this.productLargeImage = value; }

        public String[] getProductProductRatingCount() { return productProductRatingCount; }
        public void setProductProductRatingCount(String[] value) { this.productProductRatingCount = value; }

        public String[] getProductSwatchColors() { return productSwatchColors; }
        public void setProductSwatchColors(String[] value) { this.productSwatchColors = value; }

        public String[] getProductSmallImage() { return productSmallImage; }
        public void setProductSmallImage(String[] value) { this.productSmallImage = value; }

        public String[] getProductVideoLink() { return productVideoLink; }
        public void setProductVideoLink(String[] value) { this.productVideoLink = value; }

        public String[] getSkipInventory() { return skipInventory; }
        public void setSkipInventory(String[] value) { this.skipInventory = value; }

        public String[] getSkuListPrice() { return skuListPrice; }
        public void setSkuListPrice(String[] value) { this.skuListPrice = value; }

        public String[] getShipPleasantEndDate() { return shipPleasantEndDate; }
        public void setShipPleasantEndDate(String[] value) { this.shipPleasantEndDate = value; }

        public String[] getProductType() { return productType; }
        public void setProductType(String[] value) { this.productType = value; }

        public String[] getShipPleasantStartDate() { return shipPleasantStartDate; }
        public void setShipPleasantStartDate(String[] value) { this.shipPleasantStartDate = value; }

        public String[] getSkuGalleriaImage() { return skuGalleriaImage; }
        public void setSkuGalleriaImage(String[] value) { this.skuGalleriaImage = value; }

        public String[] getSkuColor() { return skuColor; }
        public void setSkuColor(String[] value) { this.skuColor = value; }

        public String[] getSkuLargeImage() { return skuLargeImage; }
        public void setSkuLargeImage(String[] value) { this.skuLargeImage = value; }

        public String[] getSkuRepositoryId() { return skuRepositoryId; }
        public void setSkuRepositoryId(String[] value) { this.skuRepositoryId = value; }

        public String[] getSkuSmallImage() { return skuSmallImage; }
        public void setSkuSmallImage(String[] value) { this.skuSmallImage = value; }

        public String[] getSkuPromoPrice() { return skuPromoPrice; }
        public void setSkuPromoPrice(String[] value) { this.skuPromoPrice = value; }

        public String[] getSkuSalePrice() { return skuSalePrice; }
        public void setSkuSalePrice(String[] value) { this.skuSalePrice = value; }

        public String[] getSkuThumbnailImage() { return skuThumbnailImage; }
        public void setSkuThumbnailImage(String[] value) { this.skuThumbnailImage = value; }

        public String[] getSortPrice() { return sortPrice; }
        public void setSortPrice(String[] value) { this.sortPrice = value; }
    }

    public class Record {
        @Expose
        @SerializedName("attributes")
        private Attributes attributes;
        @Expose
        @SerializedName("numRecords")
        private long numRecords;
        @Expose
        @SerializedName("otherClass")
        private String otherClass;
        @Expose
        @SerializedName("detailsAction")
        private DetailsAction detailsAction;
        @Expose
        @SerializedName("records")
        private Object records;

        public Attributes getAttributes() { return attributes; }
        public void setAttributes(Attributes value) { this.attributes = value; }

        public long getNumRecords() { return numRecords; }
        public void setNumRecords(long value) { this.numRecords = value; }

        public String getOtherClass() { return otherClass; }
        public void setOtherClass(String value) { this.otherClass = value; }

        public DetailsAction getDetailsAction() { return detailsAction; }
        public void setDetailsAction(DetailsAction value) { this.detailsAction = value; }

        public Object getRecords() { return records; }
        public void setRecords(Object value) { this.records = value; }
    }

    public class RemoveAllAction
    {
        @Expose
        @SerializedName("navigationState")
        private String navigationState;
        @Expose
        @SerializedName("contentPath")
        private String contentPath;
        @Expose
        @SerializedName("name_class")
        private String name_class;
        @Expose
        @SerializedName("siteRootPath")
        private String siteRootPath;
        @Expose
        @SerializedName("label")
        private Object label;

        public String getNavigationState() {
            return navigationState;
        }

        public void setNavigationState(String navigationState) {
            this.navigationState = navigationState;
        }

        public String getContentPath() {
            return contentPath;
        }

        public void setContentPath(String contentPath) {
            this.contentPath = contentPath;
        }

        public String getName_class() {
            return name_class;
        }

        public void setName_class(String name_class) {
            this.name_class = name_class;
        }

        public String getSiteRootPath() {
            return siteRootPath;
        }

        public void setSiteRootPath(String siteRootPath) {
            this.siteRootPath = siteRootPath;
        }

        public Object getLabel() {
            return label;
        }

        public void setLabel(Object label) {
            this.label = label;
        }
    }

    public class RemoveAction
    {
        @Expose
        @SerializedName("navigationState")
        private String navigationState;
        @Expose
        @SerializedName("contentPath")
        private String contentPath;
        @Expose
        @SerializedName("name_class")
        private String name_class;
        @Expose
        @SerializedName("siteRootPath")
        private String siteRootPath;
        @Expose
        @SerializedName("label")
        private Object label;

        public String getNavigationState() {
            return navigationState;
        }

        public void setNavigationState(String navigationState) {
            this.navigationState = navigationState;
        }

        public String getContentPath() {
            return contentPath;
        }

        public void setContentPath(String contentPath) {
            this.contentPath = contentPath;
        }

        public String getName_class() {
            return name_class;
        }

        public void setName_class(String name_class) {
            this.name_class = name_class;
        }

        public String getSiteRootPath() {
            return siteRootPath;
        }

        public void setSiteRootPath(String siteRootPath) {
            this.siteRootPath = siteRootPath;
        }

        public Object getLabel() {
            return label;
        }

        public void setLabel(Object label) {
            this.label = label;
        }
    }

    public class SearchCrumb
    {
        @Expose
        @SerializedName("name_class")
        private String name_class;
        @Expose
        @SerializedName("terms")
        private String terms;
        @Expose
        @SerializedName("removeAction")
        private RemoveAction removeAction;
        @Expose
        @SerializedName("correctedTerms")
        private Object correctedTerms;
        @Expose
        @SerializedName("key")
        private String key;
        @Expose
        @SerializedName("matchMode")
        private String matchMode;

        public String getName_class() {
            return name_class;
        }

        public void setName_class(String name_class) {
            this.name_class = name_class;
        }

        public String getTerms() {
            return terms;
        }

        public void setTerms(String terms) {
            this.terms = terms;
        }

        public RemoveAction getRemoveAction() {
            return removeAction;
        }

        public void setRemoveAction(RemoveAction removeAction) {
            this.removeAction = removeAction;
        }

        public Object getCorrectedTerms() {
            return correctedTerms;
        }

        public void setCorrectedTerms(Object correctedTerms) {
            this.correctedTerms = correctedTerms;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getMatchMode() {
            return matchMode;
        }

        public void setMatchMode(String matchMode) {
            this.matchMode = matchMode;
        }
    }

    public class Properties
    {
        @Expose
        @SerializedName("dimensionValueId")
        private String dimensionValueId;
        @Expose
        @SerializedName("DisplayOrder")
        private String DisplayOrder;
        @Expose
        @SerializedName("sourceId")
        private String SourceId;
        @Expose
        @SerializedName("currency")
        private String currency;

        @Expose
        @SerializedName("dGraphStrata")
        private String dGraphStrata;

        public String getDimensionValueId() {
            return dimensionValueId;
        }

        public void setDimensionValueId(String dimensionValueId) {
            this.dimensionValueId = dimensionValueId;
        }

        public String getDisplayOrder() {
            return DisplayOrder;
        }

        public void setDisplayOrder(String displayOrder) {
            DisplayOrder = displayOrder;
        }

        public String getSourceId() {
            return SourceId;
        }

        public void setSourceId(String sourceId) {
            SourceId = sourceId;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getdGraphStrata() {
            return dGraphStrata;
        }

        public void setdGraphStrata(String dGraphStrata) {
            this.dGraphStrata = dGraphStrata;
        }
    }

    public class Refinement
    {
        @Expose
        @SerializedName("multiSelect")
        private boolean multiSelect;
        @Expose
        @SerializedName("navigationState")
        private String navigationState;
        @Expose
        @SerializedName("contentPath")
        private String contentPath;
        @Expose
        @SerializedName("count")
        private long count;
        @Expose
        @SerializedName("name_class")
        private String name_class;
        @Expose
        @SerializedName("siteRootPath")
        private String siteRootPath;
        @Expose
        @SerializedName("label")
        private String label;
        @Expose
        @SerializedName("properties")
        private Properties properties;

        public boolean isMultiSelect() {
            return multiSelect;
        }

        public void setMultiSelect(boolean multiSelect) {
            this.multiSelect = multiSelect;
        }

        public String getNavigationState() {
            return navigationState;
        }

        public void setNavigationState(String navigationState) {
            this.navigationState = navigationState;
        }

        public String getContentPath() {
            return contentPath;
        }

        public void setContentPath(String contentPath) {
            this.contentPath = contentPath;
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public String getName_class() {
            return name_class;
        }

        public void setName_class(String name_class) {
            this.name_class = name_class;
        }

        public String getSiteRootPath() {
            return siteRootPath;
        }

        public void setSiteRootPath(String siteRootPath) {
            this.siteRootPath = siteRootPath;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Properties getProperties() {
            return properties;
        }

        public void setProperties(Properties properties) {
            this.properties = properties;
        }
    }

    public class Navigation
    {
        @Expose
        @SerializedName("refinements")
        public Refinement[] refinements;
        @Expose
        @SerializedName("multiSelect")
        public Boolean multiSelect;
        @Expose
        @SerializedName("type")
        public String type;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("ancestors")
        public Object[] ancestors;
        @Expose
        @SerializedName("displayName")
        public String displayName;
        @Expose
        @SerializedName("dimensionName")
        public String dimensionName;
        @Expose
        @SerializedName("whyPrecedenceRuleFired")
        public Object whyPrecedenceRuleFired;

        public Refinement[] getRefinements() {
            return refinements;
        }

        public void setRefinements(Refinement[] refinements) {
            this.refinements = refinements;
        }

        public Boolean getMultiSelect() {
            return multiSelect;
        }

        public void setMultiSelect(Boolean multiSelect) {
            this.multiSelect = multiSelect;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object[] getAncestors() {
            return ancestors;
        }

        public void setAncestors(Object[] ancestors) {
            this.ancestors = ancestors;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getDimensionName() {
            return dimensionName;
        }

        public void setDimensionName(String dimensionName) {
            this.dimensionName = dimensionName;
        }

        public Object getWhyPrecedenceRuleFired() {
            return whyPrecedenceRuleFired;
        }

        public void setWhyPrecedenceRuleFired(Object whyPrecedenceRuleFired) {
            this.whyPrecedenceRuleFired = whyPrecedenceRuleFired;
        }
    }

    public class SortOptions {
        @Expose
        @SerializedName("contentPath")
        private String contentPath;
        @Expose
        @SerializedName("navigationState")
        private String navigationState;
        @Expose
        @SerializedName("otherClass")
        private String otherClass;
        @Expose
        @SerializedName("label")
        private String label;
        @Expose
        @SerializedName("selected")
        private boolean selected;
        @Expose
        @SerializedName("siteRootPath")
        private String siteRootPath;

        public String getContentPath() { return contentPath; }
        public void setContentPath(String value) { this.contentPath = value; }

        public String getNavigationState() { return navigationState; }
        public void setNavigationState(String value) { this.navigationState = value; }

        public String getOtherClass() { return otherClass; }
        public void setOtherClass(String value) { this.otherClass = value; }

        public String getLabel() { return label; }
        public void setLabel(String value) { this.label = value; }

        public boolean getSelected() { return selected; }
        public void setSelected(boolean value) { this.selected = value; }

        public String getSiteRootPath() { return siteRootPath; }
        public void setSiteRootPath(String value) { this.siteRootPath = value; }
    }


    public class SecondaryContent
    {
        @Expose
        @SerializedName("removeAllAction")
        private RemoveAllAction removeAllAction;
        @Expose
        @SerializedName("refinementCrumbs")
        private Object[]refinementCrumbs;
        @Expose
        @SerializedName("geoFilterCrumb")
        private Object geoFilterCrumb;
        @Expose
        @SerializedName("@type")
        private String type;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("searchCrumbs")
        private SearchCrumb[] searchCrumbs;
        @Expose
        @SerializedName("rangeFilterCrumbs")
        private Object[] rangeFilterCrumbs;
        @Expose
        @SerializedName("navigation")
        private Navigation[] navigation;

        public RemoveAllAction getRemoveAllAction() {
            return removeAllAction;
        }

        public void setRemoveAllAction(RemoveAllAction removeAllAction) {
            this.removeAllAction = removeAllAction;
        }

        public Object[] getRefinementCrumbs() {
            return refinementCrumbs;
        }

        public void setRefinementCrumbs(Object[] refinementCrumbs) {
            this.refinementCrumbs = refinementCrumbs;
        }

        public Object getGeoFilterCrumb() {
            return geoFilterCrumb;
        }

        public void setGeoFilterCrumb(Object geoFilterCrumb) {
            this.geoFilterCrumb = geoFilterCrumb;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public SearchCrumb[] getSearchCrumbs() {
            return searchCrumbs;
        }

        public void setSearchCrumbs(SearchCrumb[] searchCrumbs) {
            this.searchCrumbs = searchCrumbs;
        }

        public Object[] getRangeFilterCrumbs() {
            return rangeFilterCrumbs;
        }

        public void setRangeFilterCrumbs(Object[] rangeFilterCrumbs) {
            this.rangeFilterCrumbs = rangeFilterCrumbs;
        }

        public Navigation[] getNavigation() {
            return navigation;
        }

        public void setNavigation(Navigation[] navigation) {
            this.navigation = navigation;
        }
    }

    public class Content
    {
        @Expose
        @SerializedName("title")
        public String title;
        @Expose
        @SerializedName("headerContent")
        public Object[] headerContent;
        @Expose
        @SerializedName("@type")
        public String type;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("metaKeywords")
        public String metaKeywords;
        @Expose
        @SerializedName("metaDescription")
        public String metaDescription;
        @Expose
        @SerializedName("mainContent")
        public MainContent[] mainContent;
        @Expose
        @SerializedName("secondaryContent")
        public SecondaryContent[] secondaryContent;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Object[] getHeaderContent() {
            return headerContent;
        }

        public void setHeaderContent(Object[] headerContent) {
            this.headerContent = headerContent;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMetaKeywords() {
            return metaKeywords;
        }

        public void setMetaKeywords(String metaKeywords) {
            this.metaKeywords = metaKeywords;
        }

        public String getMetaDescription() {
            return metaDescription;
        }

        public void setMetaDescription(String metaDescription) {
            this.metaDescription = metaDescription;
        }

        public MainContent[] getMainContent() {
            return mainContent;
        }

        public void setMainContent(MainContent[] mainContent) {
            this.mainContent = mainContent;
        }

        public SecondaryContent[] getSecondaryContent() {
            return secondaryContent;
        }

        public void setSecondaryContent(SecondaryContent[] secondaryContent) {
            this.secondaryContent = secondaryContent;
        }

    }

    @Expose
    @SerializedName("contents")
    public Content[] contents;
    @Expose
    @SerializedName("@type")
    private String type;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("endeca:contentPath")
    private String contentPath;
    @Expose
    @SerializedName("atg:currentSiteProductionURL")
    private String currentSiteProductionURL;
    @Expose
    @SerializedName("ruleLimit")
    private String ruleLimit;
    @Expose
    @SerializedName("endeca:siteRootPath")
    private String siteRootPath;
    @Expose
    @SerializedName("contentCollection")
    private String contentCollection;

    public Content[] getContents() {
        return contents;
    }

    public void setContents(Content[] contents) {
        this.contents = contents;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContentPath() {
        return contentPath;
    }

    public void setContentPath(String contentPath) {
        this.contentPath = contentPath;
    }

    public String getCurrentSiteProductionURL() {
        return currentSiteProductionURL;
    }

    public void setCurrentSiteProductionURL(String currentSiteProductionURL) {
        this.currentSiteProductionURL = currentSiteProductionURL;
    }

    public String getRuleLimit() {
        return ruleLimit;
    }

    public void setRuleLimit(String ruleLimit) {
        this.ruleLimit = ruleLimit;
    }

    public String getSiteRootPath() {
        return siteRootPath;
    }

    public void setSiteRootPath(String siteRootPath) {
        this.siteRootPath = siteRootPath;
    }

    public String getContentCollection() {
        return contentCollection;
    }

    public void setContentCollection(String contentCollection) {
        this.contentCollection = contentCollection;
    }
}
