package com.liverpool.demoliverpool.ui.base;

import android.support.annotation.StringRes;

/**
 * Created by andresaleman on 9/15/17.
 */

public interface MvpView
{
    void openActivityOnTokenExpire();

    void onBack();

    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(String message);

    void showMessage(@StringRes int resId);

    boolean isNetworkConnected();

    void hideKeyboard();

}
